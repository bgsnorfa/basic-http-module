const http = require("http");

const host = 'localhost';
const port = '8000';

let user = {
  email: "test@mail.com",
  password: "123456"
};

const requestListener = function (req, res) {
  res.setHeader("Content-Type", "application/json");

  if (req.url === "/" && req.method === "GET") {
    res.writeHead(200);
    res.end(JSON.stringify({
      "message": "Hello World"
    }));
    return;
  }

  if (req.url === "/login" && req.method === "POST") {
    res.writeHead(200);
    res.end(JSON.stringify({
      "message": "OK"
    }));

    return;
  }

  res.writeHead(404);
  res.end(JSON.stringify({
    "message": "Not Found"
  }));
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
  console.log(`Server is running at http://${host}:${port}`);
});